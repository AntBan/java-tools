// Nada: mostrará todo el contenido del directorio actual
// Fichero: mostrará el fichero
// Directorio: mostrará el contenido del directorio (a menos que use -d)

import java.io.File;

public class jls {

	public static void main (String args[]) {

		String files[];
		File subFiles[];

		/* Listado de ficheros en el directorio actual */
		if (args.length == 0) { 
			File path = new File ("./");

			// Si el fichero no tiene lectura, no me dejará ejecutar el programa
			if (path.canRead()) { 

				files = path.list();
				for (String f: files) {
					if (! (f.charAt (0) == '.')) // Si no es un fichero oculto
						System.out.print (f +"\t");
				}
			}
			else {
				System.out.println ("jls: cannot open directory '.': Permission denied");
				System.exit (0);
			}
				
		}
		else {

			/* Listado de ficheros en otro directorio mediante una ruta */
			File path = new File (args[0]);

			if (path.isDirectory()) {

				/* Listado del contenido del directorio */
				if (! path.canRead()) {
					System.out.println ("jls: cannot open directory '"+ path.getName() +"': Permission denied");
					System.exit (0);
				}
				subFiles = path.listFiles();
				for (File sf: subFiles) {
					if (! (sf.getName().charAt (0) == '.')) // Si no es un fichero oculto
						System.out.print (sf.getName() +"\t");
				}
			}
			/* Muestra el nombre del fichero dentro de un directorio de otra ruta */
			else if (path.isFile()) {
				System.out.println (path.getName());
			}
			else {
				System.out.println ("jls: cannot access '"+ path.getName() +"': No such file or directory");
				System.exit (0);
			}
		}

		/*
		files[] = path.list();


		for (String f: files) {
			System.out.print (f +"\t");
		}
		*/
	}
}
