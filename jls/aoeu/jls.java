// Nada: mostrará todo el contenido del directorio actual
// Fichero: mostrará el fichero
// Directorio: mostrará el contenido del directorio (a menos que use -d)

import java.io.File;

public class jls {

	public static void main (String args[]) {

		String files[];
		if (args.length == 0) { // No busco nada en concreto
			File path = new File ("./");

			if (path.canRead()) {
				files = path.list();
				for (String f: files) {
					System.out.print (f +"\t");
				}
			}
			else {
				System.out.println ("jls: cannot open directory '.': Permission denied");
			}
				
		}
		else {
			File path = new File (args[0]);
			files = path.list();
			for (String f: files) {
				System.out.print (f +"\t");
			}
		}

		/*
		files[] = path.list();


		for (String f: files) {
			System.out.print (f +"\t");
		}
		*/
	}
}
