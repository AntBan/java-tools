// Programa grep en gráfico

import java.io.*;
import java.util.Vector;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ggrep
{
	public static FileReader fr = null;

	public static void main (String[] args) {
		
		// GUI

		JFrame jf = new JFrame ("GREP GRÁFICO");
		// 3 filas, 2 columnas: izq-der, arriba-abajo
		JPanel jp = new JPanel (new GridLayout (3,2));
		jf.add (jp);

		JLabel jlb = new JLabel ("Patrón de búsqueda");
		final JTextField jtfb = new JTextField();
		jp.add (jlb);
		jp.add (jtfb);

		JLabel jlr = new JLabel ("Patrón de ruta");
		final JTextField jtfr = new JTextField();
		jp.add (jlr);
		jp.add (jtfr);

		JButton jb = new JButton ("Muestra lineas");
		jp.add (jb);
		JTextArea jta = new JTextArea();
		jp.add (jta);

		jf.setVisible (true);
		jf.pack();
		jf.setPreferredSize (new Dimension (800, 600));
		jf.setLocationRelativeTo (null); // Centro pantalla
		jf.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);

		/*
		if (args.length != 2)
		{
			System.out.println ("java ggrep <patron> FILE");
			System.exit(0);
		}
		*/

		/*
		Vector<String> v = obtieneLineas (args[0], br);

		for (String s: v) {
			//System.out.println (s);
			jta.append (s);
		}
		*/

		// Respuesta al evento clic sobre el boton (Listener "Action Listened",
		// método actionPerformed)
		jb.addActionListener (new ActionListener() {

			public void actionPerformed (ActionEvent event) {

				File f = new File (jtfr.getText());
				try {
					fr = new FileReader (f);
				}
				catch (FileNotFoundException fne) {
					System.out.println ("Se ha liado parda");
					System.out.println ("FNE: "+ fne.getMessage());
				}
				BufferedReader br = new BufferedReader (fr);
				Vector<String> v = obtieneLineas (jtfb.getText(), br);

				for (String s: v)
					//System.out.println (s);
					jta.append (s);
			}
		});
	}

	public static Vector<String> obtieneLineas (String patron, BufferedReader br) {
		String linea;
		Vector<String> v = new Vector<String>();

		try {
			while ((linea = br.readLine()) != null) {

				if (linea.contains (patron)) {
<<<<<<< HEAD
				    //System.out.println (linea); debug
=======
				    //System.out.println (linea);
>>>>>>> gui
					v.add (linea);
				}
			}
		}
		catch(IOException e) {
			System.out.println("IOException");
		}
		finally {
			try {
				if (br != null) br.close();
				if (fr != null) fr.close(); 
			}
			catch(IOException e) {
				System.out.println("IOException");
			}
		}
		return v; // Devuelve las líneas que tienen el patrón
	}
}
